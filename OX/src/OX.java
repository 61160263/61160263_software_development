import java.util.Scanner;
public class OX {
    private static Scanner kb = new Scanner(System.in);
    private static char[][] Table = new char[3][3];
    private static char turn = 'O';
    static char createTable(){
        showWelcome();
        for(int row = 0;row<Table.length;row++){
            for(int col = 0;col<Table.length;col++){
                Table[row][col]='-';
            }
        }
        return 0;
    }
    private static void showWelcome(){
        System.out.println("Welcome To OX Game");
    }
    private static char showTable(){
        System.out.println("  1 2 3");
        for(int row = 0; row<Table.length; row++){
            System.out.println((row+1)+" "+Table[row][0]+" "+Table[row][1]+
                                " "+Table[row][2]);
        }
        return 0;
    }
    private static char changeTurn(){
        if(turn == 'O'){
            turn='X';
            return turn;
        }else{
            turn='O';
            return turn;
        }
    }
    private static void showTurn(){
        System.out.println("Turn "+turn);
    }
    private static int input(){
        System.out.println("Please Input Row Col : ");
        while(true){
            int Row = kb.nextInt();
            int Col = kb.nextInt();
            if(Row<1 | Row>3 | Col<1 | Col>3){
                System.out.println("Please Input Row Col Again : ");
            }else if(Table[Row-1][Col-1]=='-' & Table[Row-1][Col-1]!=turn){
                Table[Row-1][Col-1]=turn;
                break;  
            }else 
                System.out.println("Please Input Row Col Again : ");
        }
        return 0;
    }
    private static boolean checkLandscape(){
        for(int row = 0; row<Table.length; row++){
            if(Table[row][0]==turn&Table[row][1]==turn&Table[row][2]==turn){
                showTable(); 
                System.out.println("Player "+turn+" Win");
                return true; 
            }
        }
        return false;
    }
    private static boolean checkVertical(){
        for(int col = 0; col<Table.length; col++){
            if(Table[0][col]==turn&Table[1][col]==turn&Table[2][col]==turn){
                showTable(); 
                System.out.println("Player "+turn+" Win");
                return true; 
            }
        }
         return false;
    }
    private static boolean checkObliqueRight(){
            if(Table[0][0]==turn&Table[1][1]==turn&Table[2][2]==turn){
                    showTable(); 
                    System.out.println("Player "+turn+" Win");
                    return true; 
            }
        return false;
    }
    private static boolean checkObliqueLeft(){
            if(Table[0][2]==turn&Table[1][1]==turn&Table[2][0]==turn){
                    showTable(); 
                    System.out.println("Player "+turn+" Win");
                    return true; 
            }
        return false;
    }
    private static boolean checkWin(){
        if(checkVertical()==true){
            return true;
        }else if(checkLandscape()==true){
            return true;
        }else if(checkObliqueRight()==true){
            return true;
        }else if(checkObliqueLeft()==true){
            return true;
        }
        return false;
    }
    private static boolean draw(){
        int count=0;
        for(int row = 0; row<Table.length;row++){
            for(int col = 0;col<Table.length;col++){
                if(Table[row][col]=='O'|Table[row][col]=='X'){
                  count++; 
                }if(count==9){
                    showTable();
                    System.out.println("Draw");
                    return true;
                }
            }
        }
        return false;
    } 
    private static boolean checkAll(){
        if(checkWin()==true){
            return true;
        }
        if(draw()==true){
            return true;
        }
        return false;
    }
    public static void main(String[]args){
        createTable();
        while(true){
            showTable();
            showTurn();
            input();
            checkWin();
            if(checkAll()==true){
                break;
            }
            changeTurn();  
        }
    }
}